import tensorflow as tf
import numpy as np
import os, sys
import cv2

import model_zoo
from config import FLAGS

import argparse

from gtts import gTTS
import os
from darknet.python.darknet import *
os.environ["CUDA_VISIBLE_DEVICES"]="1"


with open('category.txt') as f:
    lines = map(lambda x: x.strip(), f.readlines())

ix2label = dict(zip(range(len(lines)), lines))

cwd = os.getcwd()
model_path = os.path.join(cwd, 'save_model', 'jester-finetune')


class TFModel:
    def __init__(self):
        parser = argparse.ArgumentParser(description="test TF on a single video")
        parser.add_argument('--video_length', type=int, default=60)
        parser.add_argument('--fps', type=int, default=30)
        parser.add_argument('--cam', type=int, default=10)
        parser.add_argument('--mode', type=str, default='IR')
        self.args = parser.parse_args()
        self.pipeline = None # for IR

        if self.args.cam==10:
            import pyrealsense2 as rs
            self.pipeline = pipeline = rs.pipeline()
            config = rs.config()

            # depth_aligned_to_color  rs.stream.depth
            config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, self.args.fps)  # set depth
            config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, self.args.fps)  # set depth
            config.enable_stream(rs.stream.infrared, 1, 640, 480, rs.format.y8, self.args.fps)  # right imager

            profile = pipeline.start(config)

            depth_sensor = profile.get_device().first_depth_sensor()
            depth_sensor.set_option(rs.option.emitter_enabled, 0)
            depth_sensor.set_option(rs.option.enable_auto_exposure, 1)


            for _ in range(30):
                pipeline.wait_for_frames()


            depth_scale = depth_sensor.get_depth_scale()


        # load yolo v3(tiny) and meta data
        #self.yolo = load_net("./darknet/cfg/yolov3-tiny-voc.cfg", "./darknet/cfg/yolov3-tiny-voc_210000.weights", 0)
        #self.meta = load_meta("./darknet/cfg/voc.data")
        self.yolo = load_net("./darknet/cfg/yolov3.cfg", "./darknet/cfg/yolov3.weights", 0)
        self.meta = load_meta("./darknet/cfg/coco.data")

        self.inputs = tf.placeholder(dtype=tf.float32, shape=[None, None, 224, 224, 3])

        # build IC3D net
        self.net = model_zoo.I3DNet(inps=self.inputs, n_class=len(ix2label),
                               pretrained_model_path=None,
                               final_end_point='Logits', dropout_keep_prob=1.0,
                               is_training=False, scope='v/SenseTime_I3D')

        # logits from IC3D net
        self.pred = tf.nn.softmax(self.net(inputs=self.inputs))

        # gpu config
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        # open session
        self.sess = tf.Session(config=config)
        self.sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()

        ckpt = tf.train.latest_checkpoint(model_path)

        if ckpt:
            print 'restore from {}...'.format(ckpt)
            saver.restore(self.sess, ckpt)

    def calc_depth_return_coord(self, depth_frame, r):
        depth_center = []
        for i in range(0, len(r)):
            _, _, (x, y, w, h) = r[i]
            depth_distance = depth_frame.get_distance(int(x), int(y))
            if depth_distance == 0 and i != 0: depth_distance = depth_center[i-1]
            depth_center.append(depth_distance)
        #depth_min = np.min(depth_center)
        print('person distance: ', depth_center)
        _, _, (x, y, w, h) = r[np.argmin(depth_center)]
        return x, y, w, h

    def get_frames(self, num, fps, cam=0):
        if not cam == 10:
            cap = cv2.VideoCapture(cam)
            cap.set(cv2.CAP_PROP_FPS, fps)
            cap.set(cv2.CAP_PROP_AUTOFOCUS, False)

        seq = []
        x=-0.5;y=-0.5;w=-1;h=-1;

        while True:
            if not cam == 10:
                _, frame = cap.read()
            elif cam == 10:
                # Wait for a coherent pair of frames: depth and color
                frames = self.pipeline.wait_for_frames()


                depth_frame = frames.get_depth_frame()

                ir_frame1 = frames.get_infrared_frame(1)

                if not depth_frame or not ir_frame1:
                    continue


                # convert depth images to numpy arrays
                depth_image = np.asanyarray(depth_frame.get_data())

                # convert infrared images to numpy arrays
                ir_image1 = np.asanyarray(ir_frame1.get_data())

                ir_image1 = cv2.cvtColor(ir_image1, cv2.COLOR_GRAY2BGR)

                #frame = cv2.cvtColor(color_image, cv2.COLOR_GRAY2BGR)
                if self.args.mode == 'IR':
                    frame = ir_image1
                elif self.args.mode == 'RGB':
                    frame = np.asanyarray(frames.get_color_frame().get_data())

            if not os.path.exists('./tmp'):
                os.makedirs('./tmp')
            cv2.imwrite('./tmp/0.jpg',frame)

            r = detect(self.yolo, self.meta, './tmp/0.jpg')

            if len(r) == 1 and not seq:
                _,_,(x,y,w,h) = r[0] # coco
            elif len(r) >= 2 and not seq:
                x, y, w, h = self.calc_depth_return_coord(depth_frame, r)


            ymin, ymax, xmin, xmax = int(y-h/2), int(y+h/2), int(x-w/2-15), int(x+w/2+15)

            if ymin>0 and xmin>0 \
                    and ymin<ymax and xmin<xmax:
                frame = frame[ymin:ymax,xmin:xmax]


            # aspect ratio
            ratio = float(frame.shape[0])/float(frame.shape[1])

            if ratio>1.0:       # sitting
                frame = frame[:int(frame.shape[0]/2)]

            elif ratio>1.5:     # standing
                frame = frame[:int(frame.shape[0]/2.5)]


            cv2.imshow('cam-{}'.format(cam), frame)
            cv2.moveWindow('cam-{}'.format(cam), 20, 20)
            cv2.waitKey(1)

            frame = cv2.resize(frame, (224, 224))

            seq.append(frame)

            if len(seq) > num:
                if not cam == 10:
                    cap.release()
                return np.expand_dims(seq, axis=0)

    def run_demo_wrapper(self):
        frames = self.get_frames(num = self.args.video_length, fps= self.args.fps, cam=self.args.cam)



        predictions = self.sess.run(self.pred, feed_dict={self.inputs: frames})

        mask = map(lambda x: int(ix2label[int(x.argmax(axis=-1))]!='Doing other things'), predictions[0])

        # casting
        mask = np.expand_dims(np.expand_dims(mask, axis=0), 2)

        predicted_label = map(lambda x: ix2label[int(x.argmax(axis=-1))], predictions[0])

        if predicted_label.count('Doing other things') > int(self.args.video_length*0.8):
            return 'Doing other things', 'Null'

        # apply mask to predictions
        predictions_masked = np.mean(mask*predictions, axis=1)

        str_prediction = ix2label[int(predictions_masked.argmax(axis=-1).squeeze())]

        return str_prediction.strip(), predictions_masked.max()


    def __del__(self):
        if self.pipeline:
            self.pipeline.stop()

from gtts import gTTS
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str)
args = parser.parse_args()

text = ' '.join(args.name.split('_'))
tts = gTTS(text=text)
tts.save(args.name+'.mp3')

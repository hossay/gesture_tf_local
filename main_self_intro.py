from TFModel import TFModel
import requests
import json

if __name__ == '__main__':

    model = TFModel()
    while True:
        result, confidence = model.run_demo_wrapper()

        if confidence>0.3:
            if result == 'Stop Sign':
                print('--sending to conroller {}...---'.format(result))
                try:
                    requests.get(
                        'http://155.230.104.191:3001/api/v1/actions/action/{}/{}'.format('home', json.dumps({'question_on':True})))
                except:
                    pass